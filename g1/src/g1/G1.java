/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g1;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author RuiCo
 */
public class G1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        Query query = new Query();
        char selector;
        do {
            System.out.println("Insira:\n A para ver o montabte anual \n S para sair");
            // ler o caracter incerido
            selector = scanner.next().charAt(0);
            //trasformar o caracter em maiuscula 
            selector = Character.toUpperCase(selector);
            switch (selector) {
                case 'A':
                    query.vendasEmpresa();
                    System.out.println("O montante anual da empresa é "+query.getMontante()+"€");
                    break;
                case 'B':
                    break;
                //sair do programa
                case 'S':
                    System.exit(0);
                    break;
                //Caso contrario
                default:
                    selector = 'b';
                    break;
            }
        } while (selector == 'b');

    }

}
